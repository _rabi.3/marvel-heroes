import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer } from '../reducer';

const configureStore = (intialState = {}) => {
  const logger = createLogger();
  const middlewares = [thunk, logger];
  return createStore(reducer, intialState, composeWithDevTools(applyMiddleware(...middlewares)));
};

export default configureStore;