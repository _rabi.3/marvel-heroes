import crypto from 'crypto';
import axios from 'axios';
import { loadMarvels, loadMarvelInfo } from '../actions';

const publicKey = '298bab46381a6daaaee19aa5c8cafea5';
const privateKey = 'b0223681fced28de0fe97e6b9cd091dd36a5b71d';
const ts = `${Math.round(new Date() / 1000)}`;
const plainText = ts.concat(privateKey, publicKey);
const hash = crypto.createHash('md5').update(`${plainText}`).digest('hex');

const heroesListURL = `http://gateway.marvel.com:80/v1/public/characters?ts=${ts}&apikey=${publicKey}&hash=${hash}`;

const marvelInfosURL = (id) => `http://gateway.marvel.com:80/v1/public/characters/${id}?ts=${ts}&apikey=${publicKey}&hash=${hash}`;

export const requestMarvels = () => dispatch =>  axios.get(heroesListURL)
  .then( ({ data }) => data)
  .then(({ data: {results = []} }) =>  dispatch(loadMarvels(results)))
  .catch(console.error);

export const requestMarvelInfo = (marvelId) => dispatch => axios.get(marvelInfosURL(marvelId))
  .then( ({ data }) => data)
  .then(({ data: {results = []} }) => dispatch(loadMarvelInfo(results)))
  .catch(console.error);
