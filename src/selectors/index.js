import { createSelector } from 'reselect';
import { prop } from 'ramda';

export const getMarvels = createSelector(prop('marvels'), marvels => marvels);

export const getCurrentMarvel = createSelector(prop('currentMarvel'), currentMarvel => currentMarvel);
