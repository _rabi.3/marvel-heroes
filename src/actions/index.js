const LOAD_MARVELS = 'LOAD_MARVELS';
const LOAD_MARVEL = 'LOAD_MARVEL';
const CLOSE = 'CLOSE';

export const loadMarvels = marvels => (
  {
    type: LOAD_MARVELS,
    payload: {marvels} 
  }
);

export const loadMarvelInfo = marvel => (
  {
    type: LOAD_MARVEL, 
    payload: {currentMarvel: marvel}
  }
);

export const close = () => (
  {type: CLOSE}
);