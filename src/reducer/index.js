export const reducer = (state = { marvels: [] }, {type, payload}) => {
  switch (type){
    case 'LOAD_MARVELS':
      return { ...state, marvels: payload.marvels}
    case 'LOAD_MARVEL':
      return { ...state, currentMarvel: payload.currentMarvel}
    case 'CLOSE':
      return { ...state, currentMarvel: undefined}
    default: 
      return state;
  }
}