import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'ramda';
import { withStyles, Grid, Card, CardHeader, CardMedia, CardContent, Button } from '@material-ui/core';

const styles = {
  container:{
    padding: 20,
  },
  card: {
    minHeight: 350,
    minWidth: 350,
  }, 
  media: {
    height: 0,
    paddingTop: '56.25%'
  }
}

const HeroesList = ({ classes, marvels, loadMarvel }) => (
  <Grid container direction="row" justify="space-evenly" alignItems="center" spacing={16} className={classes.container}>
    { 
      marvels ? map(hero => (
        <Grid item key={hero.id} >
          <Grid container>
            <Card className={classes.card}>
              <CardMedia className={classes.media} image={`${hero.thumbnail.path}.${hero.thumbnail.extension}`}/>
              <CardHeader title={hero.name} />
              <CardContent><Button variant="contained" color="primary" onClick={() => loadMarvel(hero.id)}>Details</Button></CardContent>
            </Card>
          </Grid>
        </Grid>
      ), marvels) 
      : null
    }
  </Grid>
)

HeroesList.propTypes = {
  classes: PropTypes.object.isRequired,
  marvels: PropTypes.array.isRequired,
  loadMarvel: PropTypes.func.isRequired,
}

export default withStyles(styles)(HeroesList);