import React from 'react';
import PropTypes from 'prop-types';
import { map, addIndex } from 'ramda';
import { Typography, Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core';


const HeroTable = ({ title, comics}) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell> <Typography variant="h6" id="tableTitle">{title}</Typography></TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {addIndex(map)((comic, index) =>(
        <TableRow key={index} >
          <TableCell>{comic.name}</TableCell>
        </TableRow>
      ), comics)}
    </TableBody>
  </Table>
)

HeroTable.propTypes = {
  title: PropTypes.string.isRequired,
  comics: PropTypes.array.isRequired
}

export default HeroTable;