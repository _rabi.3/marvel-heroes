import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Grid, Card, CardMedia } from '@material-ui/core';
import Table from './HeroTable';
import Header from './Header';

const styles = {
  container:{
    padding: 25,
  },
  media: {
    height: 350,
    minWidth: 150,
    paddingTop: '56.25%',
  }
}

const HeroInfos = ({ currentMarvel, classes, close }) => (
  <Grid container onClick={() => close()}>
    {currentMarvel ?
      <Grid container direction="row" className={classes.container} spacing={16}> 
        
        <Grid item xs={3}>
          <Card><CardMedia image={`${currentMarvel.thumbnail.path}.${currentMarvel.thumbnail.extension}`} className={classes.media} /></Card>
        </Grid>

        <Grid item xs={9}>
          <Grid container direction="column">
            <Grid item>
              <Header name={currentMarvel.name} description={currentMarvel.description}/>
            </Grid>
            <Grid item> 
              <Table comics={currentMarvel.comics.items} title="COMICS" />
            </Grid>
            <Grid item> 
              <Table comics={currentMarvel.series.items} title="SERIES" />
            </Grid>
          </Grid>
        </Grid>

      </Grid>
      : null}
  </Grid>
    
)

HeroInfos.propTypes = {
  currentMarvel: PropTypes.object,
  hero: PropTypes.object,
  close: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(HeroInfos);