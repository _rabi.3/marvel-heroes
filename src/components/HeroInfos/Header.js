import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Paper, Typography, } from '@material-ui/core';

const styles = {
  container:{
    padding:20,
  }
}

const Header = ({ name, description, classes }) => (
  <Paper className={classes.container}>
    <Typography variant="h4" component="h3"> {name} </Typography>
    <Typography component="p">{description}</Typography>
  </Paper>
)

Header.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Header);