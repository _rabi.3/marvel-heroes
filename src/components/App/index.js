import React from 'react';
import PropTypes from 'prop-types';
import { head } from 'ramda';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import {requestMarvels, requestMarvelInfo } from '../../request';
import { close } from '../../actions';
import HeroesList from '../HeroesList'
import HeroInfos from '../HeroInfos';
import { getMarvels, getCurrentMarvel } from '../../selectors';

const mapStateToProps = state => ({
  marvels: getMarvels(state),
  currentMarvel: getCurrentMarvel(state),
});

const mapDispatchToProps = dispatch => ({
  loadMarvels: () => dispatch(requestMarvels()),
  loadMarvel: marvelId => dispatch(requestMarvelInfo(marvelId)),
  close: () => dispatch(close()),
});

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  lifecycle({
    componentDidMount(){
      this.props.loadMarvels()
    },
  }),
);

const App = ({ marvels, currentMarvel, loadMarvel, close }) => (
  marvels ? (currentMarvel ? <HeroInfos currentMarvel={head(currentMarvel)} close={close} /> 
  : <HeroesList marvels={marvels} loadMarvel={loadMarvel} />)
  : 'Loading...'  
)

App.propTypes = {
  marvels: PropTypes.array,
  close: PropTypes.func.isRequired,
  currentMarvel: PropTypes.array,
  loadMarvel: PropTypes.func.isRequired,
};

export default enhance(App);